# cnsdose/php-zephir

https://cloud.docker.com/u/cnsdose/repository/docker/cnsdose/php-zephir

```
docker pull cnsdose/php-zephir:7.4 # 7.1/7.2/7.3/..., leave it out for the latest one
docker run --rm -it -v /path/to/local/project:/opt/cnsd cnsdose/php-zephir '/bin/bash'
# in docker
cd /opt/cnsd
zephir compile
```
